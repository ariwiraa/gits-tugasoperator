package main

import (
	"fmt"
	"strings"

	humanize "github.com/dustin/go-humanize"
)

func main() {
	var idr, usd, euro float64
	var numMenu int

	fmt.Println("===SubMenu===")
	fmt.Println("1. Rupiah ke Dollar\n2. Rupiah ke Euro\n3. Konversi GB Pounds")
	fmt.Print("Masukan angka untuk pilih konversi: ")
	fmt.Scan(&numMenu)

	if numMenu == 1 {
		fmt.Printf("Masukan jumlah rupiah : Rp.")
		fmt.Scan(&idr)
		usd = idr / 14251.55
		euro = idr / 16874.87
		fmt.Printf("IDR : %s,00 \n", FormatRupiah(idr))
		fmt.Printf("USD : $ %.2f \n", usd)
	} else if numMenu == 2 {
		fmt.Printf("IDR : %s,00 \n", FormatRupiah(idr))
		fmt.Printf("EURO : € %.2f \n", euro)
	} else if numMenu == 3 {
		var gbp, knut, sickle, galleon int

		fmt.Print("Masukan jumlah GB Pounds :")
		fmt.Scan(&gbp)

		knut = gbp * 100 // konversi gbp ke knut
		fmt.Println("Jumlah knut yang didapat =", knut, "Knut")

		galleon = knut / 493   // tukar knut ke galleon
		sisaKnut := knut % 493 // sisa dari hasil pertukan galleon
		fmt.Println("Hasil penukaran mendapatkan =", galleon, "Galleon")

		sickle = sisaKnut / 29      // sisaknut ditukar ke sickle
		sisaSickle := sisaKnut % 29 // perhitungan sisa sickle
		fmt.Println("sisa ditukar menjadi =", sickle, "Sickle")
		fmt.Println("keping knut yang tersisa =", sisaSickle, "Knut")
	} else {
		fmt.Println("Pilihan tidak ada")
	}

}

func FormatRupiah(amount float64) string {
	humanizeValue := humanize.CommafWithDigits(amount, 0)
	stringValue := strings.Replace(humanizeValue, ",", ".", -1)
	return "Rp " + stringValue
}
